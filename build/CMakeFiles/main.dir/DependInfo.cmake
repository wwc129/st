# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/wwc129/code/main.cpp" "/home/wwc129/code/build/CMakeFiles/main.dir/main.cpp.o"
  "/home/wwc129/code/tests/mock_data.cpp" "/home/wwc129/code/build/CMakeFiles/main.dir/tests/mock_data.cpp.o"
  "/home/wwc129/code/tests/test_alglib_lsfit.cpp" "/home/wwc129/code/build/CMakeFiles/main.dir/tests/test_alglib_lsfit.cpp.o"
  "/home/wwc129/code/tests/test_st.cpp" "/home/wwc129/code/build/CMakeFiles/main.dir/tests/test_st.cpp.o"
  "/home/wwc129/code/utils/api.cpp" "/home/wwc129/code/build/CMakeFiles/main.dir/utils/api.cpp.o"
  "/home/wwc129/code/utils/calc_bg.cpp" "/home/wwc129/code/build/CMakeFiles/main.dir/utils/calc_bg.cpp.o"
  "/home/wwc129/code/utils/fits_io.cpp" "/home/wwc129/code/build/CMakeFiles/main.dir/utils/fits_io.cpp.o"
  "/home/wwc129/code/utils/helper_types.cpp" "/home/wwc129/code/build/CMakeFiles/main.dir/utils/helper_types.cpp.o"
  "/home/wwc129/code/utils/lsfit.cpp" "/home/wwc129/code/build/CMakeFiles/main.dir/utils/lsfit.cpp.o"
  "/home/wwc129/code/utils/psf.cpp" "/home/wwc129/code/build/CMakeFiles/main.dir/utils/psf.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../includes/cfitsio"
  "../includes/alglib"
  "../utils"
  "../tests"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
